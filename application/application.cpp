#include "applicationHook.h"
#include "led/led.hpp"
#include <chrono>
#include <tuple>
#include <message/Message.hpp>
#include <message/Queue.hpp>

class Dimmer : public message::Receiver<std::tuple<LED::Instance,int>>
{
public:
    Dimmer(message::ReceiverQueue& q) : message::Receiver<std::tuple<LED::Instance,int>>(q)
    {
        
    }
    virtual void receive(message::Message<std::tuple<LED::Instance,int>>& m) final override
    {
        LED::value(std::get<0>(m.data), std::get<1>(m.data));
        message::Message<std::tuple<LED::Instance,int>>::send(*this, message::Event::UPDATE, 
            std::make_tuple(std::get<0>(m.data), (std::get<1>(m.data)+1)%100), std::chrono::milliseconds(10));
    }
};

void startApplication()
{
    LED::init(LED::Instance::small);
    LED::init(LED::Instance::large);
   
    message::Queue<32, 16> queue;
    
    Dimmer dimmer(queue);
    message::Message<std::tuple<LED::Instance,int>>::send(dimmer, message::Event::UPDATE, 
        std::make_tuple(LED::Instance::large, 10), std::chrono::milliseconds(10));
    
    while(true)
    {
        queue.dispatch();
    }
    
}
