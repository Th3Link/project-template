#ifndef __LED_HPP__
#define __LED_HPP__

namespace LED
{
    enum class Instance
    {
        small, large, count
    };
    void init(Instance);
    void value(Instance, unsigned int percent);
    unsigned int value(Instance);
}

#endif //__LED_HPP__
