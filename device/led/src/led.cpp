#include "led/led.hpp"
#include "stm32f4xx_hal.h"

#include <array>
#include <tuple>

using channel = uint32_t;
using timer = TIM_TypeDef*;

std::array<std::tuple<channel, timer, unsigned int>, static_cast<unsigned int>(LED::Instance::count)> leds
{
    std::make_tuple(TIM_CHANNEL_1, TIM3, 0),
    std::make_tuple(TIM_CHANNEL_2, TIM3, 0)
};

void LED::init(LED::Instance instance)
{
    LED::value(instance, 0);
}

void LED::value(LED::Instance instance, unsigned int percent)
{
    TIM_OC_InitTypeDef sConfigOC {0}; 
    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = 2000 * percent / 100;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    
    auto i = static_cast<unsigned int>(instance);
    std::get<2>(leds[i]) = percent;
    
    TIM_HandleTypeDef tim;
    tim.Instance = std::get<1>(leds[i]);
    HAL_TIM_PWM_ConfigChannel(&tim, &sConfigOC, std::get<0>(leds[i]));
    HAL_TIM_PWM_Start(&tim, std::get<0>(leds[i])); 
    HAL_Delay(5);
}

unsigned int LED::value(LED::Instance instance)
{
    auto i = static_cast<unsigned int>(instance);
    return std::get<2>(leds[i]);
}
