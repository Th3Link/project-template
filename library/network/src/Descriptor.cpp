//include package intern
#include <network/Descriptor.hpp>

//include package extern

//include std::*

namespace network {

Descriptor::Descriptor(Protocol protocol, unsigned short port) :
    protocol(protocol),
    port(port)
{
    
}

bool Descriptor::hasSameService(const Descriptor& nd) {
	return (port == nd.port) && (protocol == nd.protocol);
}
}
