#include <network/Network.hpp>
#include <network/Server.hpp>
#include <network/Connection.hpp>

int main()
{
    network::Network::init();
    
    network::Server<1,100> echoServer(network::Protocol::TCP, 4444);
    network::Error e = network::Error::AWESOME;
    
    echoServer.poll();
    
    for (auto& connection : echoServer.connections)
    {
        if (connection.buffer.size() > 10)
        {
            connection.send(connection.buffer, e);
            connection.buffer.clear();
        }
    }
    
    return 0;
}
