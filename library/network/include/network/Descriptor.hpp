#ifndef __NETWORKDESCRIPTOR_HPP__
#define __NETWORKDESCRIPTOR_HPP__

//include package intern

//include package extern

//include std::*

#define IP_ARRAY_LEN 4
namespace network {
class Descriptor
{
    public:
        enum Protocol
        {  
            PROTOCOL_TCP, PROTOCOL_UDP, PROTOCOL_ICMP
        };
        Descriptor(Protocol protocol, unsigned short port);
        bool hasSameService(const Descriptor& nd);
        Protocol protocol;
        unsigned short port;
    protected:
        
    private:
        
};
}
#endif //__NETWORKDESCRIPTOR_HPP__
