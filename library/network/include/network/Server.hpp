#ifndef __NETWORK_SERVER_HPP__
#define __NETWORK_SERVER_HPP__

//include package intern
#include "Network.hpp"
#include "Connection.hpp"

//include package extern

//include std::*
#include <cstdint>
#include <etl/vector.h>

namespace network {

//template<unsigned int>
//class Connection;

template<unsigned int MAX_CONNECTIONS, unsigned int BUFFER_SIZE>
class Server
{
    public:
        Server(Protocol protocol, uint16_t port) : protocol(protocol), port(port)
        {
            
        }
        Server(const Server&) = delete; // copy constructor
        Server(const Server&&) = delete; // move constructor
        Server& operator=(const Server&) = delete; // copy assignment operator
        Server& operator=(const Server&&) = delete; // move assignment operator
        ~Server()
        {
            
        }
        void poll()
        {
            //accept();
            
            for (auto& connection : connections)
            {
                network::Error e = network::Error::AWESOME;
                connection.recv(e);
            }
        }
    private:
        Protocol protocol;
        uint16_t port;
    public:
        etl::vector<Connection<BUFFER_SIZE>, MAX_CONNECTIONS> connections;
};
}
#endif //__NETWORK_SERVER_HPP__
