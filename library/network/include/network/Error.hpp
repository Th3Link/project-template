#ifndef __NETWORK_ERROR_HPP__
#define __NETWORK_ERROR_HPP__

namespace network {
enum class Error
{
    FAIL_NO_SPACE,
    FAIL_NOT_IMPLEMENTED,
    FAIL_NETWORK,
};
}

#endif //__NETWORK_ERROR_HPP__
