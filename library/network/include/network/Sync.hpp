#ifndef __NETWORKSYNC_HPP__
#define __NETWORKSYNC_HPP__

//include package intern
#include <network/Connection.hpp>

//include package extern

//include std::*

namespace network {

class Sync
{
public:
    virtual ~Sync();
    virtual void requestNewConnectionSync(std::shared_ptr<Connection>) = 0;
    virtual void requestConnectionRemoteClosedSync(Connection*) = 0;
    virtual void poll() = 0;
};
}
#endif //__NETWORKSYNC_HPP__
