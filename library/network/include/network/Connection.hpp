#ifndef __NETWORK_CONNECTION_HPP__
#define __NETWORK_CONNECTION_HPP__

//include package intern

//include package extern

//include std::*
#include <vector>
#include <array>
#include <cstdint>
#include <etl/vector.h>
namespace network {

template<unsigned int BUFFER_SIZE>
class Connection
{
    public:
        Connection();
        Connection(const Connection&) = delete; // copy constructor
        Connection(const Connection&&) = delete; // move constructor
        Connection& operator=(const Connection&) = delete; // copy assignment operator
        Connection& operator=(const Connection&&) = delete; // move assignment operator
        virtual ~Connection();
        bool operator==(const Connection& c);
        void send(etl::vector<uint8_t, BUFFER_SIZE>& in, Error& error)
        {
            
        }
        void recv(Error& error)
        {
            
        }
        void close(Error& error);
        void kill();
        etl::vector<uint8_t, BUFFER_SIZE> buffer;
    };
}
#endif //__NETWORKCONNECTION_HPP__
