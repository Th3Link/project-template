#include <chrono>
#include "stm32f4xx_hal.h"

extern "C" int gettimeofday(struct timeval* tv, void* tz) {
    auto tick = HAL_GetTick();
    tv->tv_sec  = tick/1000;
    tv->tv_usec = (tick%1000)*1000;
    return 0;
}
