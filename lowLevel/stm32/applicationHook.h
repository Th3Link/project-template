#ifndef __APPLICATION_HOOK_H__
#define __APPLICATION_HOOK_H__

#ifdef __cplusplus
extern "C" {
#endif

void startApplication();

#ifdef __cplusplus
}
#endif

#endif //__APPLICATION_HOOK_H__
